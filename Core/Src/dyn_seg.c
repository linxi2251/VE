//
// Created by linxi on 2020/11/1.
//

#include "dyn_seg.h"

uint8_t table_dyn[] = {
        0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90, 0x88, 0x83, 0xc6, 0xa1, 0x86, 0x8e
};
void DYN_SEG_Disp(uint8_t wei, uint8_t data) {
    //位选
    HAL_GPIO_WritePin(GPIOA, wei, (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOA, (~wei & 0xff), (GPIO_PinState) RESET);

    // 送数据
    HAL_GPIO_WritePin(GPIOC, table_dyn[data], (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOC, (~table_dyn[data] & 0xff), (GPIO_PinState) RESET);
}

// 将int型的数据写入到buff数组中
void Data_to_Buff(uint8_t *buff, uint32_t  num) {
    buff[0] = num / 10000000;
    buff[1] = num / 1000000 % 10;
    buff[2] = num / 100000 % 10;
    buff[3] = num / 10000 % 10;
    buff[4] = num / 1000 % 10;
    buff[5] = num / 100 % 10;
    buff[6] = num / 10 % 10;
    buff[7] = num % 10;
}
