//
// Created by linxi on 2020/11/1.
//

#include "static_seg.h"

uint8_t table_static[] = {0x77, 0x14, 0xB3, 0xB6, 0xD4, 0xE6, 0xE7, 0x34,
                   0xF7, 0xF6, 0xF5, 0xC7, 0x63, 0x97, 0xE3, 0xE1, 0xff};

void Static_SEG_Disp(uint8_t i, uint8_t state) {
    HAL_GPIO_WritePin(GPIOA, table_static[i], (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOA, (~table_static[i] & 0xff), (GPIO_PinState) RESET);
    if (state) HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, (GPIO_PinState) RESET);
}