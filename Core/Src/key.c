//
// Created by linxi on 2020/11/1.
//

#include "key.h"

uint8_t KEY_Scan() {
    uint8_t key_val = 0;

    ROW_EN(ROW1);
    COL_Scan(&key_val, 1, 2, 3, 4);

    ROW_EN(ROW2);
    COL_Scan(&key_val, 5, 6, 7, 8);

    ROW_EN(ROW3);
    COL_Scan(&key_val, 9, 10, 11, 12);

    ROW_EN(ROW4);
    COL_Scan(&key_val, 13, 14, 15, 16);

    return key_val;
}

void ROW_EN(uint16_t row) {
    HAL_GPIO_WritePin(GPIOB, row, (GPIO_PinState) RESET);
    HAL_GPIO_WritePin(GPIOB, (~row & 0xff00), (GPIO_PinState) SET);
}

void COL_Scan(uint8_t *key_val, uint8_t col1, uint8_t col2, uint8_t col3, uint8_t col4) {
    if (~(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15) | HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14) |
         HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13) | HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12))) {
        HAL_Delay(5);

        if (!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)) {
            *key_val = col1;
        }
        if (!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)) {
            *key_val = col2;
        }
        if (!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13)) {
            *key_val = col3;
        }
        if (!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12)) {
            *key_val = col4;
        }
    }
}
