//
// Created by linxi on 2020/10/31.
//

#include "led.h"
uint8_t group_LED[] = {
        initLed,
        led1,
        led2,
        led3,
        led4,
        led5,
        led6,
        led7,
        led8
};

void LED_Init() {
    HAL_GPIO_WritePin(GPIOA, ledAll, (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, (GPIO_PinState) RESET);
}

void LED_Disp(__uint8_t led) {
    HAL_GPIO_WritePin(GPIOA, led, (GPIO_PinState) RESET);
    HAL_GPIO_WritePin(GPIOA, ~led, (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, (GPIO_PinState) SET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, (GPIO_PinState) RESET);
}