//
// Created by linxi on 2020/11/1.
//

#ifndef VE_KEY_H
#define VE_KEY_H

#include "stm32f1xx_hal.h"

#define ROW1 GPIO_PIN_11
#define ROW2 GPIO_PIN_10
#define ROW3 GPIO_PIN_9
#define ROW4 GPIO_PIN_8

uint8_t  KEY_Scan();
void ROW_EN(uint16_t row);
void COL_Scan(uint8_t *key_val, uint8_t col1, uint8_t col2, uint8_t col3, uint8_t col4);
#endif //VE_KEY_H
