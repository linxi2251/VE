//
// Created by linxi on 2020/11/1.
//

#ifndef VE_DYN_SEG_H
#define VE_DYN_SEG_H

#include "stm32f1xx_hal.h"

#define WEI1 GPIO_PIN_0
#define WEI2 GPIO_PIN_1
#define WEI3 GPIO_PIN_2
#define WEI4 GPIO_PIN_3
#define WEI5 GPIO_PIN_4
#define WEI6 GPIO_PIN_5
#define WEI7 GPIO_PIN_6
#define WEI8 GPIO_PIN_7

void DYN_SEG_Disp(uint8_t wei, uint8_t data);
void Data_to_Buff(uint8_t *buff, uint32_t num);
#endif //VE_DYN_SEG_H
