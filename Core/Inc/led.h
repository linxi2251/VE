//
// Created by linxi on 2020/10/31.
//

#ifndef VE_LED_H
#define VE_LED_H

#include "stm32f1xx_hal.h"

#define initLed 0
#define led1 GPIO_PIN_0
#define led2 GPIO_PIN_1
#define led3 GPIO_PIN_2
#define led4 GPIO_PIN_3
#define led5 GPIO_PIN_4
#define led6 GPIO_PIN_5
#define led7 GPIO_PIN_6
#define led8 GPIO_PIN_7
#define ledAll led1 | led2 | led3 | led4 | led5 | led6 | led7 | led8

void LED_Init();
void LED_Disp(__uint8_t led);
#endif //VE_LED_H
