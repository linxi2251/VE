//
// Created by linxi on 2020/11/1.
//

#ifndef VE_STATIC_SEG_H
#define VE_STATIC_SEG_H

#include "stm32f1xx_hal.h"

void Static_SEG_Disp(uint8_t i, uint8_t state);
#endif //VE_STATIC_SEG_H
